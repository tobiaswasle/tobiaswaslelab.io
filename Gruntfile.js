module.exports = function(grunt) {

  grunt.initConfig({
    'sftp-deploy': {
      build: {
        auth: {
          authKey: 'akw',
          host: 'ideerly.com',
          port: 22
        },
        src: './public/',
        dest: '/var/www/virtual/wasle/ideerly.com/',
        progress: true
      }
    },
  }
)
grunt.loadNpmTasks('grunt-sftp-deploy');

grunt.registerTask('deploy', ['sftp-deploy']);


};
